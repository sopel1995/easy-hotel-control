#! /usr/bin/python3
import json
import send_email
from slacker import Slacker
import configparser
import time
import datetime
import os
import _thread

calendar_with_action = []
config = configparser.ConfigParser()
config.read('config.ini')


class date_time_action(object):
    def __init__(self, datetime, function, parameters='default'):
        self.datetime = datetime
        self.function = function
        self.parameters = parameters

    def run(self):
        self.function(self.parameters)

    def get_datetime(self):
        return self.datetime


def add_to_calendar(action):
    iter = 0
    for x in calendar_with_action:
        print(x.datetime)
        print('&')
        print(action.datetime)
        if (x.datetime < action.datetime):
            iter += 1
        else:
            break
    calendar_with_action.insert(iter, action)



def slack(text):
    _thread.start_new_thread(slack_write, (text, config))

def slack_write(text,config):
    slack_token = config['slack']['slack_token']
    sc = Slacker(slack_token)

    sc.chat.post_message(
        channel= config['slack']['channel'],
        text=text,
    )

def run():
    # add_to_calendar(date_time_action(datetime.datetime(2019, 3, 16, 18, 3, 0, 0), send_local_email, {"subject":"SUCCESS", "content":"1"}))
    # add_to_calendar(date_time_action(datetime.datetime(2019, 3, 16, 18, 20, 0, 0), send_local_email, {"subject":"SUCCESS", "content":"2"}))
    #add_to_calendar(date_time_action(datetime.datetime(2019, 5, 23, 15, 19, 0, 0), slack, "OK"))
    os.environ['TZ'] = 'Poland'
    time.tzset()

    slack("DOCKER EHC testing slack ;D")
    while True:
        now = datetime.datetime.now()
        if (calendar_with_action):
            first = calendar_with_action[0].datetime
            #print(str(first) + " & " + str(now))
            if first < now:
                slack("Time to action")
                calendar_with_action.pop(0).run()

        print("CHECK CALENDAR: " + str(now))
        time.sleep(5)
    # print("SLACK")

if __name__ == '__main__':
    run()

