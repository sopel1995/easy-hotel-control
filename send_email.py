import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import configparser
import db
import numpy
from email import encoders
from email.mime.base import MIMEBase
import json
import _thread




config = configparser.ConfigParser()
config.read('config.ini')
config_content = configparser.ConfigParser()
config_content.read('email_content.ini')




def send_email(parameters):
    port = config['email']['port']
    smtp_server = config['email']['smtp_server']
    sender_email = config['email']['sender_email']
    password = config['email']['password']
    receiver_email = parameters['receiver_email']
    subject = parameters['subject']
    content = parameters['content']
    _thread.start_new_thread(send_email_hard,(sender_email, receiver_email, password, subject, content, smtp_server, port) )


def send_email_hard(sender_email, receiver_email, password, subject, content, smtp_server, port):
    message = MIMEMultipart()
    message["Subject"] = subject
    message["From"] = sender_email
    message["To"] = receiver_email
    message.attach(MIMEText('<html><body><center>''<p><img src="cid:0"></p>' +
                            '<h1>'+content+'</h1>' +
                        '</center></body></html>', 'html', 'utf-8'))

    with open('ehc.png', 'rb') as f:
        # set attachment mime and file name, the image type is png
        mime = MIMEBase('image', 'png', filename='ehc.png')
        # add required header data:
        mime.add_header('Content-Disposition', 'attachment', filename='ehc.png')
        mime.add_header('X-Attachment-Id', '0')
        mime.add_header('Content-ID', '<0>')
        # read attachment file content into the MIMEBase object
        mime.set_payload(f.read())
        # encode with base64
        encoders.encode_base64(mime)
        # add MIMEBase object to MIMEMultipart object
        message.attach(mime)

    server = smtplib.SMTP(smtp_server, port)
    server.starttls()
    server.login(sender_email, password)
    text = message.as_string()
    server.sendmail(sender_email, receiver_email,text)
    server.quit()

def send_email_new_reservation_to_client(email):
    parameters = {
        "receiver_email": email,
        "subject": 'Reservation confirmation',
        "content": "Confirmation of room reservation "
    }
    #TODO: Room parameters, date.
    send_email.send_email(parameters)


def send_email_reservation_confirmation(res):


    room_description = "Description" #(db.action_with_db('procedureRoomInfo', [int(res['roomLabel']), 6], False))[0]['roomDescription']


    content = str(config_content['accept_reservation']['content']).replace('{roomLabel}', res['roomLabel']).replace('{reservationStart}', res['reservationStart']).replace('{reservationEnd}', res['reservationEnd']).replace('{room_description}', room_description).replace('{totalPrice}', res['totalPrice'])
    parameters = {
        "receiver_email": res['clientEmail'],
        "subject": 'Reservation confirmation',
        "content": content
    }
    send_email(parameters)


def send_email_to_cook():
    email = db.ation_with_db("allUsersMails", 5, False)
    number_of_quests = db.ation_with_db("procedureReturnPresentPeopleNumber", [], False)
    for mail in email:
        parameters = {
            "receiver_email": mail,
            "subject": "Info about meals",
            "content": "Please prepare "+ str(number_of_quests) + "servings today."
        }


def send_email_to_cleaners():
    #TODO: allUsersMails(paramPermission)
    email = db.ation_with_db("allUsersMails", 3, False)
    rooms_cleaning_needed = db.ation_with_db("showRoomWhereCleaningNeeded", [], False)
    number_of_cleaners = len(rooms_cleaning_needed)
    rooms_cleaning_needed = numpy.array_split(numpy.array(number_of_cleaners), 6)
    clean_tasks = zip(email, rooms_cleaning_needed)
    for task in clean_tasks:
        parameters = {
            "receiver_email": task[0],
            "subject": "Info about dirty rooms",
            "content": "Please clean room: " + str(task[1]) + " today."
        }


def send_email_daily_raport():
    #email = db.ation_with_db("allUsersMails", 2, False)
    email = ["mateuszsobkowiak1995@gmail.com"]
    number_of_quests = db.action_with_db("makeDailyReport", None, False)
    for mail in email:


        parameters = {
            "receiver_email": mail,
            "subject": "Info about clients",
            "content": "Number of clients on hotel "+ str(number_of_quests) + " today."
        }


def send_test(mail):
    parameters = {
        "receiver_email": mail,
        "subject": "Info about clients",
        "content": "Test png"
    }
    send_email(parameters)


def send_email_to_receprtions(content):
    #email = db.ation_with_db("allUsersMails", 1, False)
    email = ["mateuszsobkowiak1995@gmail.com"]

    for mail in email:
        parameters = {
            "receiver_email": mail,
            "subject": "Info about clients",
            "content": content
        }
        send_email(parameters)

def send_email_goodbye(res):
    print("Godbye")
    mail = res['clientEmail']
    content = f"Thank you, {res['clientName']} for the stay.<br> We invite you again. <br><br>Regards,<br>EHC system"
    parameters = {
        "receiver_email": mail,
        "subject": "Goodbye",
        "content": content
    }
    send_email(parameters)


