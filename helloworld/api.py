from tornado_json.requesthandlers import APIHandler
from tornado_json import schema
from http import HTTPStatus
import fb
import log
import db
import send_email
import time_acction
import  datetime
import run
import json
import datetime

import os
def get_access(self, email, service_blocked)->bool:
    user = db.action_with_db_http(self, 'selectUser', [email], False)[0]
    if(user):
        user_permision = int(list(user.values())[2])     #user['Permission description']
        if(service_blocked[0] == 0):
            return True
        if(user_permision in service_blocked):
            return False
        else:
            return True



class dotdict(dict):
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__
def read_root():
    return {"Hello": "World"}
def veryfication(self):
    tok = self.get_argument('token', None)
    if (tok == None):
        self.set_status(401)
        return self.error(message="NO TOKEN", code=HTTPStatus.BAD_REQUEST)
    try:
        veryfication = fb.veryfication_user_with_token(tok)
    except ValueError as e:
        self.set_status(401)
        return self.error(message="Account no veryfity email", code=HTTPStatus.UNAUTHORIZED)
    if (veryfication.find('@') != -1):
        return veryfication
    else:
        self.set_status(401)
        self.error(message="AUTHENTICATION FAILED: "+veryfication, code = HTTPStatus.UNAUTHORIZED)
        return veryfication
    # return "easyhotelcontrol@gmail.com"  #DEBUG



# Usefull api function
class check_system(APIHandler):
    @schema.validate(
        output_schema={
            "status": "string",
        }
    )

    def get(self):
        log.log("CHECK SYSTEM")
        hostname = "google.com"  # example
        response = os.system("ping -c 1 " + hostname)
        if(response == 0):
            response = "OK"
        else:
            response = "ERROR"

        if(db.db_cursor_connect() == None):
            database = "ERROR"
        else:
            database = "OK"
        ver_email = veryfication(self)
        run.add_to_calendar(run.date_time_action(datetime.datetime(2019, 3, 16, 18, 3, 0, 0), run.slack, {"CHECK SYSTEM"}))
        send_email.send_email_daily_raport()
        return {"STATE": "OK", "DATABASE": database, "INTERNET_CONNECTION": response, "user": ver_email}


class client(APIHandler):

    @schema.validate(
        output_schema={
            "Client name": "string",
            "Client surname": "string",
            "Client email": "string",
            "PESEL": "string",

        }
    )
    def get(self):
        email_connect = veryfication(self)
        param = []
        param.append(self.get_argument('paramPESEL'))

        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'procedureClientSelectByPESEL', param, False)
        else:
            self.set_status(401)
            return "NO PERMISSION"

    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "paramClName": "string",
            "paramClSurname": "string",
            "paramEMail": "string",
            "paramPhNumber": "string",
            "paramStreet": "string",
            "paramAddress": "string",
            "paramPostalCode": "string",
            "paramCity": "string",
            "paramPESEL": "string",
            "paramIDNum": "string"
        },
    )
    def post(self):
        email_connect = veryfication(self)
        args =[]
        for param in self.body:
            args.append(self.body[param])

        if (get_access(self, email_connect, [0])):

            return db.action_with_db_http(self, 'procedureClientAdd', args, True)
        else:
            self.set_status(401)
            return "NO PERMISSION"


class clients(APIHandler):

    @schema.validate(
        output_schema={
            "clientID": "string",
            "clientPESEL": "string"
        }
    )
    def get(self):
        email_connect = veryfication(self)
        param = []

        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'procedureSelectAllPESEL', param, False)
        else:
            self.set_status(401)
            return "NO PERMISSION"


class room(APIHandler):

    @schema.validate(
        output_schema={
            "Floor number": "string",
            "Room number": "string",
            "Room ID": "string",
            "Last cleaning": "string",
            "Last service": "string",
            "Cleaning needed": "string",
            "Service needed": "string",
        }
    )
    def get(self):
        email_connect = veryfication(self)
        param =[]
        param.append(self.get_argument('paramLabel'))

        if (get_access(self, email_connect, [0])):
            result = db.action_with_db_http(self, 'procedureRoomInfoState', param, False)
            try:
                result = result[0]
            except IndexError:
                result = result
            return result
        else:
            return "NO PERMISSION"

    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "paramFloorNumber": "int",
            "paramLabel": "int",
            "paramDayPrice": "int",
            "paramRoomDesc": "string",
            "paramCapacity": "int"
        },
    )
    def post(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])

        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'procedureAddRoom', args, True)
        else:
            self.set_status(401)
            return "NO PERMISSION"


    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "paramLabel": "int",
        },
    )
    def delete(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])

        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'deleteRoom', args, True)
        else:
            self.set_status(401)
            return "NO PERMISSION"
    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "paramLabel": "int",
            "roomState": "int"
        },
    )
    def put(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])

        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'procedureSetValidRoomState', args, True)
        else:
            self.set_status(401)
            return "NO PERMISSION"


class rooms(APIHandler):

    @schema.validate(
        output_schema={
            "roomLabel": "int",
            "roomID": "int",
            "dayPrice": "int",
            "roomDescription": "string",
            "roomStatus": "int",
            "roomCapacity": "int"
        }
    )
    def get(self):
        email_connect = veryfication(self)
        param = []
        param.append(self.get_argument('paramFloor'))


        if (get_access(self, email_connect, [0])):
            user = db.action_with_db_http(self, 'selectUser', [email_connect], False)[0]
            user_permision = int(list(user.values())[2])
            param.append(user_permision)
            return db.action_with_db_http(self, 'procedureTakeRoomOnFloor', param, False)
        else:
            self.set_status(401)
            return "NO PERMISSION"


class reservation(APIHandler):

    @schema.validate(
        output_schema={
            "clientID": "int",
            "clientName": "string",
            "clientSurname": "string",
            "clientEmail": "string",
            "pesel": "string",
            "roomLabel": "int",
            "floorNumber": "int",
            "totalPrice": "int",
            "reservationID": "iny",
            "roomID": "int",
            "reservationStart": "string",
            "reservationEnd": "string"
        }
    )
    def get(self):
        email_connect = veryfication(self)
        param = []
        param.append(self.get_argument('paramResID'))

        if (get_access(self, email_connect, [0])):
            result = db.action_with_db_http(self, 'selectReservation', param, False)
            try:
                result = result[0]
            except IndexError:
                result = result
            return result
        else:
            self.set_status(401)
            return "NO PERMISSION"

    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "paramClName": "string",
            "paramClSurname": "string",
            "paramEMail": "string",
            "paramPhNumber": "string",
            "paramStreet": "string",
            "paramAddress": "string",
            "paramPostalCode": "string",
            "paramCity": "string",
            "paramPESEL": "string",
            "paramIDNum": "string",
            "paramLabel": "int",
            "paramStart": "string",
            "paramEnd": "string",
            "paramAdvance": "int",
            "paramPeopleAmmount": "int",
        },
    )
    def post(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])

        if (get_access(self, email_connect, [0])):
            result = db.action_with_db_http(self, 'procedureClientAdd', args[0:10], True)
            if(result != "Taken" and not bool(db.action_with_db_http(self, 'procedureClientSelectByPESEL', [args[8]], False))):
                self.set_status(507)
                return result
            self.set_status(200)
            args.insert(11, args[8])    # Insert PESEL
            result = db.action_with_db_http(self, 'makeRoomReservation', args[10:], True)
            if(result != "Taken"):
                self.set_status(507)
                return result

            info_res = db.action_with_db_http(self, 'markRoomAsReserved', [args[10]], False)
            info_res = db.action_with_db_http(self, 'selectReservation', [info_res[0]['resID']], False)
            info_res = info_res[0]
            #send_email.send_email_reservation_confirmation(info_res)
            run.add_to_calendar(run.date_time_action(time_acction.str_to_datetime(info_res['reservationStart'])- datetime.timedelta(minutes=3), time_acction.check_room_ready, info_res))
            run.add_to_calendar(run.date_time_action(time_acction.str_to_datetime(info_res['reservationEnd']) + datetime.timedelta(minutes=3), time_acction.check_out_client, info_res))

            return info_res
        else:
            self.set_status(401)
            return "NO PERMISSION"


    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "paramResID": "int",
        },
    )
    def delete(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])
        if(get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'procedureDeleteReservation', args, True)
        else:
            self.set_status(401)
            return "NO PERMISSION"


class availablerooms(APIHandler):

    @schema.validate(
        output_schema={
            "state": "string",

        }
    )
    def get(self):      # DO PRZETESTOWANIA CO ZWRACA
        email_connect = veryfication(self)
        param = []
        param.append(str(self.get_argument('paramStart')))
        param.append(str(self.get_argument('paramEnd')))

        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'procedureChooseAvailableRoomsAllFloors', param, False)
        else:
            self.set_status(401)
            return "NO PERMISSION"


class user(APIHandler):

    @schema.validate(
        output_schema={
            "paramClName": "string",
            "paramClSurname": "string",
            "paramEmail": "string",
            "paramPhNumber": "string",
            "paramAddress": "string",
            "paramPESEL": "string",
            "paramIDNum": "string",
        }
    )
    def get(self):
        email_connect = veryfication(self)
        param = []
        param.append(self.get_argument('paramEmail'))

        if (get_access(self, email_connect, [0])):
            result = db.action_with_db_http(self, 'selectUser', param, False)
            try:
                result = result[0]
            except IndexError:
                result = result
            return result
        else:
            self.set_status(401)
            return "NO PERMISSION"

    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "paramName": "string",
            "paramEmail": "string",
            "paramPermission": "string",
        },
    )
    def post(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])

        if(get_access(self, email_connect, [0])):
            try:
                fb.add_user(args[0], args[1])
                result = db.action_with_db_http(self, 'addEhcUser', args[1:], True)
                return result
            except ValueError as e:
                self.set_status(400)
                self.error(message=e.args[0], code=HTTPStatus.BAD_REQUEST)

            except ValueError as e:
                return str(e)

        else:
            self.set_status(401)
            return "NO PERMISSION"


    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "userEmail": "string",
        },
    )
    def delete(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])
            if(get_access(self, email_connect, [0])):
                return db.action_with_db_http(self, 'deleteEhcUser', args, True)
            else:
                self.set_status(401)
            return "NO PERMISSION"


class clean_services(APIHandler):
    @schema.validate(
        output_schema={
            "roomNumber": "int",
            "lastCleaning": "string",
            "cleaningNeeded": "int"
        }
    )
    def get(self):
        email_connect = veryfication(self)
        param = []
        param.append(self.get_argument('paramLabel'))
        param.append(2)

        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'procedureRoomInfo', param, False)
        else:
            self.set_status(401)
            return "NO PERMISSION"

    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "paramLabel": "int",
        },
    )
    def post(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])

        if(get_access(self, email_connect, [0])):
            try:

                result = db.action_with_db_http(self, 'changeCleaningState', args, True)
                return result
            except ValueError as e:
                self.set_status(400)
                self.error(message=e.args[0], code=HTTPStatus.BAD_REQUEST)

            except ValueError as e:
                return str("cos")

        else:
            self.set_status(401)
            return "NO PERMISSION"


    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "paramLabel": "int",
        },
    )
    def delete(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])
            if(get_access(self, email_connect, [0])):
                run.slack(f"Room {args[0]} is clined.")
                return db.action_with_db_http(self, 'updateRoomLastCleaning', [args[0], email_connect], True)
            else:
                self.set_status(401)
            return "NO PERMISSION"


class room_services(APIHandler):
    @schema.validate(
        output_schema={
            "roomNumber": "int",
            "lastCleaning": "string",
            "cleaningNeeded": "int"
        }
    )
    def get(self):
        email_connect = veryfication(self)
        param = []
        param.append(self.get_argument('paramLabel'))
        param.append(4)

        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'procedureRoomInfo', param, False)
        else:
            self.set_status(401)
            return "NO PERMISSION"

    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "paramLabel": "int",
        },
    )
    def post(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])

        if(get_access(self, email_connect, [0])):
            try:

                result = db.action_with_db_http(self, 'changeServiceState', args, True)
                return result
            except ValueError as e:
                self.set_status(400)
                self.error(message=e.args[0], code=HTTPStatus.BAD_REQUEST)

            except ValueError as e:
                return str("cos")

        else:
            self.set_status(401)
            return "NO PERMISSION"


    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "paramLabel": "int",
        },
    )
    def delete(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])
            if(get_access(self, email_connect, [0])):
                run.slack(f"Room {args[0]} is servised.")
                return db.action_with_db_http(self, 'updateRoomLastService', [args[0], email_connect], True)
            else:
                self.set_status(401)
            return "NO PERMISSION"


class users(APIHandler):
    @schema.validate(
        output_schema={
            "paramClName": "string",
            "paramClSurname": "string",
            "paramEmail": "string",
            "paramPhNumber": "string",
            "paramAddress": "string",
            "paramPESEL": "string",
            "paramIDNum": "string",
        }
    )
    def get(self):
        email_connect = veryfication(self)
        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'selectAllUsers', None, False)
        else:
            self.set_status(401)
            return "NO PERMISSION"


class reservations(APIHandler):

    @schema.validate(
        output_schema={
            "clientID": "int",
            "clientName": "string",
            "clientSurname": "string",
            "clientEmail": "string",
            "pesel": "string",
            "roomLabel": "int",
            "floorNumber": "int",
            "totalPrice": "int",
            "reservationID": "iny",
            "roomID": "int",
            "reservationStart": "string",
            "reservationEnd": "string"
        }
    )
    def get(self):
        email_connect = veryfication(self)
        param = []

        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'selectAllReservations', param, False)
        else:
            self.set_status(401)
            return "NO PERMISSION"


class reservations_past_now(APIHandler):

    @schema.validate(
        output_schema={
            "clientID": "int",
            "clientName": "string",
            "clientSurname": "string",
            "clientEmail": "string",
            "pesel": "string",
            "roomLabel": "int",
            "floorNumber": "int",
            "totalPrice": "int",
            "reservationID": "iny",
            "roomID": "int",
            "reservationStart": "string",
            "reservationEnd": "string"
        }
    )
    def get(self):
        email_connect = veryfication(self)
        param = []

        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'selectReservationsPastNow', param, False)
        else:
            self.set_status(401)
            return "NO PERMISSION"


class configuraton(APIHandler):

    @schema.validate(
        output_schema={
            "paramFloorNumber": "int",
            "paramLabel": "int",
            "paramDayPrice": "int",
            "paramRoomDesc": "string",
        }
    )
    def get(self):
        email_connect = veryfication(self)
        param = []
        param.append(self.get_argument('paramResID'))
        param.append(self.get_argument('paramSurname'))

        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'selectReservation', param, False)
        else:
            self.set_status(401)
            return "NO PERMISSION"

    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "time_raport": "string",
            "paramLabel": "int",
            "paramName": "int",
            "paramSurname": "string",
            "paramStart": "string",
            "paramEnd": "string",
            "paramAdvance": "int",
            "paramAdvanceStatus": "int",
        },
    )
    def post(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])

        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'makeRoomReservation', args, True)
        else:
            self.set_status(401)
            return "NO PERMISSION"


class active_reservation(APIHandler):

    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "activeReservation": "int",
            "paramLabel": "int",
            "paramResID": "int"


        },
    )
    def post(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])

        if (get_access(self, email_connect, [0])):
            if (args[0] == 0):
                resp_0 = db.action_with_db_http(self, 'procedureCheckOutOfEHC', [args[2]], True)
                resp_1 = db.action_with_db_http(self, 'procedureSetValidRoomState', [args[1]], True)
                res = db.action_with_db_http(self, 'selectReservation', [args[2]], False)
                try:
                    res = res[0]
                except IndexError:
                    res = res
                send_email.send_email_goodbye(res)
            else:
                resp_0 = db.action_with_db_http(self, 'procedureActivateReservation', [args[2]], True)
                resp_1 = db.action_with_db_http(self, 'procedureActivateReservedRoom', [args[1]], True)

            if(resp_0 == "Taken" and resp_1 == "Taken"):
                resp = "Taken"
            else:
                resp = f"{resp_0} : {resp_1}"
            return resp
        else:
            self.set_status(401)
            return "NO PERMISSION"

class mark_paid(APIHandler):

    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "paramResID": "int",

        },
    )
    def post(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])

        if (get_access(self, email_connect, [0])):
            return db.action_with_db_http(self, 'markAdvanceAsPaid', args, True)
        else:
            self.set_status(401)
            return "NO PERMISSION"
    @schema.validate(
        output_schema={
            "state": "string",
        },
        input_schema={
            "paramResID": "int",
        },
    )
    def delete(self):
        email_connect = veryfication(self)
        args = []
        for param in self.body:
            args.append(self.body[param])
            if(get_access(self, email_connect, [0])):
                return db.action_with_db_http(self, 'markAdvanceNotPaid', args, True)
            else:
                self.set_status(401)
            return "NO PERMISSION"

#Test function
class fb_all_user(APIHandler):

    @schema.validate(
        output_schema={
                "state": "string",
        }
    )
    def get(self):

        return fb.all_users()


class add_event(APIHandler):


    @schema.validate(
        output_schema={
            "state": "string"
        }
    )

    def get(self):
        dt = datetime.datetime.now() + datetime.timedelta(seconds=3)
        return run.add_to_calendar(run.date_time_action(dt, run.slack,"EVENT RUN"))
class daily_raport(APIHandler):


    @schema.validate(
        output_schema={
            "state": "string"
        }
    )

    def get(self):
        return time_acction.daily_info()
