import logging
import mysql.connector
from mysql.connector import errorcode
import log

def action_with_db_http(self, procedure, arg, commit):
    log.log('action with db, procedure: '+procedure+ "arg: "+str(arg))
    db = None
    cursor = None
    try:
        db = mysql.connector.connect(user='root', password='cr',
                                     host='mysql-server',
                                     database='tables')
        cursor = db.cursor()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)

    if(cursor == None):
        self.set_status(507)
        db.close()

        return "Error connect db"

    try:
        if arg == None:
            cursor.execute("CALL "+procedure+"();")
        else:
            str_args = ""
            for element in arg:
                if (isinstance(element, str)):
                    str_args += '\''+ element+'\','
                else:
                    str_args +=str(element) + ','
            str_args = str_args[0:-1] #usuwamy przecinek
            cursor.execute("CALL "+procedure+"("+ str_args+");")
            if(commit):
                db.commit()
        response = get_results(cursor)


    except mysql.connector.Error as e:
        response = {"error": str(e)}
        self.set_status(507)
        print(e)
        cursor.close()
        db.close()
        return response

    cursor.close()
    db.close()
    # if(response == []):
    #     response = "EMPTY"
    return response

def action_with_db(procedure, arg, commit):
    log.log('action with db, procedure: '+procedure+ "arg: "+str(arg))
    db = None
    cursor = None
    try:
        db = mysql.connector.connect(user='root', password='cr',
                                     host='mysql-server',
                                     database='tables')
        cursor = db.cursor()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)

    if(cursor == None):
        return "Error connect db"

    try:
        if arg == None:
            cursor.execute("SELECT * FROM clients")
        else:
            str_args = ""
            for element in arg:
                if (isinstance(element, str)):
                    str_args += '\''+ element+'\','
                else:
                    str_args +=str(element) + ','
            str_args = str_args[0:-1] #usuwamy przecinek
            cursor.execute("CALL "+procedure+"("+ str_args+");")
            if(commit):
                db.commit()
        response = get_results(cursor)


    except mysql.connector.Error as e:
        response = {"error": str(e)}
        print(e)

    cursor.close()
    db.close()
    return response

def get_results(db_cursor):
    json_data = []
    if(db_cursor.description == None):
        return "Taken"
    desc = [d[0] for d in db_cursor.description]

    for res in db_cursor.fetchall():
        iter = 0
        result = {}
        for param in res:
            result[desc[iter]] = str(param)
            iter += 1
        json_data.append(result)
    return json_data

def db_cursor_connect():
    try:
        db = mysql.connector.connect(user='root', password='cr',
                                     host='mysql-server',
                                     database='tables')
        cursor = db.cursor()
        return cursor
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
        return None