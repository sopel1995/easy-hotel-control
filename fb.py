import firebase_admin
from firebase_admin import credentials
from firebase_admin import auth
import json
import send_email
import db
import configparser


config = configparser.ConfigParser()
config.read('config.ini')

def init_firebase():

    if (not len(firebase_admin._apps)):
        cred = credentials.Certificate('easyhotelcontrol-bcd58-firebase-adminsdk-vt8be-0ecc3d67e9.json')
        fb = firebase_admin.initialize_app(cred)

def add_user(name, email):

    try:
        init_firebase()
    except:
        return False
    print("go")
    try:
        user = auth.create_user(
            email=email,
            email_verified=False,
            password="te$t#2019",
            display_name=name,
        )
        print('Sucessfully created new user: {0}'.format(user.uid))
        # link = auth.generate_email_verification_link(email)
        # parameters = {
        #     "receiver_email": email,
        #     "subject": 'Veryfication account',
        #     "content": "Please click to verify your account: "+link
        # }
        # send_email.send_email(parameters)
        restart_password_with_email(email)
        return user.uid


    except auth.AuthError as e:
        arg = str(e.args[0]).replace('\n', '')
        arg = arg.replace(' ', '')
        index = arg.find('"message')
        arg = arg[index:arg.find(',',index)]
        arg = arg.replace('\\','')
        arg = json.loads('{'+arg+'}')['message']
        raise ValueError(arg)


def veryfication_user_with_token(token):
    try:
        decoded_token = auth.verify_id_token(token)
        print(decoded_token)
        email = decoded_token['email']
        if(auth.get_user(decoded_token['uid']).email_verified):
            return email
        else:
            raise ValueError("No veryfication email")
    except ValueError as e:
        print(e)
        return str(e)

def veryfication_user_with_email(email):
    try:
        user = auth.get_user_by_email(email)
        print('Successfully fetched user data: {0}'.format(user.uid))
        print("AUTH: "+ str(auth.get_user(user.uid).email_verified))
        if(auth.get_user(user.uid).email_verified):
            return email
        else:
            return ("No veryfication email")
    except auth.AuthError as e:
        return str(e)

def all_users():
    # Start listing users from the beginning, 1000 at a time.
    init_firebase()
    page = auth.list_users()
    while page:
        for user in page.users:
            print('User: ' + user.uid)
        # Get next batch of users.
        page = page.get_next_page()

    # Iterate through all users. This will still retrieve users in batches,
    # buffering no more than 1000 users in memory at a time.
    user_list =[]
    for user in auth.list_users().iterate_all():
        print('User: ' + user.uid)
        user_list.append(str(user.email + str(user.email_verified)))

    return  user_list

def restart_password_with_email(email):
    link = auth.generate_password_reset_link(email)
    parameters = {
        "receiver_email": email,
        "subject": 'Veryfication account',
        "content": "Please click to reset password your account: " + link
    }
    send_email.send_email(parameters)

def check_admin_account():
    email = config['email']['admin_email']
    password = config['email']['admin_password']
    try:
        init_firebase()
        user = auth.get_user_by_email(email)
        print('Successfully fetched user data: {0}'.format(user.uid))
    except auth.AuthError as e:     #No user
        print(e)
        add_user('admin',password,email)
        db.action_with_db( 'addEhcUser', ['admin', password, email], True)
        print('Account created')
    except:
        return False
    print("go")
