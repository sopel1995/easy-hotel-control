import db
import send_email
import configparser
import datetime
import run

config = configparser.ConfigParser()
config.read('email_content.ini')
def check_room_ready(res):
    run.slack("Check room ready")
    #   Check clear
    need_clean = bool(db.action_with_db('procedureRoomInfo', [res['roomLabel'], 2], False)[0]['cleaningNeeded'])
    need_service = bool(db.action_with_db('procedureRoomInfo', [res['roomLabel'], 4], False)[0]['serviceNeeded'])
    if(need_clean == 0 & need_service == 0):
        return True
    else:
        content = f"Problem with room {res['roomLabel']}. \n"
        if(need_clean):
            content += f"Room isn't clear.\n"
        elif(need_service):
            content += f"Room need service.\n"
        content += f"Next reservation is {res['reservationStart'] }."

        send_email.send_email_to_receprtions(content)
        return False

def check_out_client(res): #advanceStatus
    # TODO: Write function
    #selectReservation
    act_res_info = db.action_with_db('selectReservation', res['reservationID'], False)[0]
    reservationStatus = act_res_info['reservationStatus']
    advanceStatus = act_res_info['advanceStatus']
    content =""
    if(reservationStatus != 2 or advanceStatus ):
        content += f"Klient {act_res_info['clientName']} {act_res_info['clientSurname']}" \
            f" o mailu {act_res_info['clientEmail']} "
        if(reservationStatus != 2):
            content +=  "wymeldował się "
        else:
            content += "nie wymeldował się "
        if(advanceStatus != 1):
            content += "i nie opłacił pobytu."
        else:
            content += "lecz opłacił pobyt."
        send_email.send_email_to_receprtions(content)

def str_to_datetime(str_time) ->datetime:
    return datetime.datetime.strptime(str_time, '%Y-%m-%d %H:%M:%S')
def daily_info():
    # TODO: Send info about rooms, clients,
    print("daily raport")
    info = db.action_with_db_http('makeDailyReport', None, False)
    return info