import tornado.ioloop

from tornado_json.routes import get_routes
from tornado_json.application import Application
import helloworld
import run
import log
import fb
import threading


class threadTornado(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
    def run(self):
        print("RUN THREAD")
        run.run()

print("IsOkGit")



def main():
    # Pass the web app's package the get_routes and it will generate
    #   routes based on the submodule names and ending with lowercase
    #   request handler name (with 'handler' removed from the end of the
    #   name if it is the name).
    # [("/api/helloworld", helloworld.api.HelloWorldHandler)]
    fb.init_firebase()
    try:
        fb.check_admin_account()
    except ValueError as e:
        print(e)

    routes = get_routes(helloworld)
    threadTornado().start()
    log.log("Start system EHC")
    # Create the application by passing routes and any settings
    application = Application(routes=routes, settings={})

    # Start the application on port 8888
    application.listen(8888)
    try:
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().stop()


if __name__ == '__main__':
    main()

