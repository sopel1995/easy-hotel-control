import datetime

def log(message):
    now = datetime.datetime.now()
    f = open("log.txt", "a+")
    log = now.isoformat() + ': ' + message + '\n'
    print(log)
    f.write(log)
    f.close()